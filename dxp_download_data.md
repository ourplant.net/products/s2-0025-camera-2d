Here, you will find an overview of the open source informmation of this product. The detailed information can be found within the repository at [GitLab](https://gitlab.com/ourplant.net/products/s2-0025-camera-2d).

|document|download options|
|:-----|-----:|
|operating manual           |[de](https://gitlab.com/ourplant.net/products/s2-0025-camera-2d/-/raw/main/01_operating_manual/S2-0025_A1_Betriebsanleitung.pdf) / [en](https://gitlab.com/ourplant.net/products/s2-0025-camera-2d/-/raw/main/01_operating_manual/S2-0025_A1_Operating%20Manual.pdf)|
|assembly drawing           |[de](https://gitlab.com/ourplant.net/products/s2-0025-camera-2d/-/raw/main/05_assembly_drawing/s2-0025-C_ZNB_camera.pdf)|
|circuit diagram            |[de/en](https://gitlab.com/ourplant.net/products/s2-0025-camera-2d/-/raw/main/02_circuit_drawing/S2-0025-EPLAN-B.pdf)|
|maintenance instructions   |[de](https://gitlab.com/ourplant.net/products/s2-0025-camera-2d/-/raw/main/04_maintenance_instructions/S2-0025_A_Wartungsanweisungen.pdf) / [en](https://gitlab.com/ourplant.net/products/s2-0025-camera-2d/-/raw/main/04_maintenance_instructions/S2-0025_A_Maintenance_instructions.pdf)|
|spare parts                |[de](https://gitlab.com/ourplant.net/products/s2-0025-camera-2d/-/raw/main/03_spare_parts/S2-0025_B_EVL_Camera.pdf) / [en](https://gitlab.com/ourplant.net/products/s2-0025-camera-2d/-/raw/main/03_spare_parts/S2-0025_B_SWP_Camera.pdf)|

<!-- 2021 (C) Häcker Automation GmbH -->
